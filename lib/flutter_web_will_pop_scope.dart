/// A library with the necessary for facilitating a
/// [WillPopScope] widget in Flutter Web that hooks into the browser's
/// back and refresh button.
library flutter_web_will_pop_scope;

/// A Calculator.
class Calculator {
  /// Returns [value] plus 1.
  int addOne(int value) => value + 1;
}
